const fs = require("fs");
module.exports.problem1 = (dirPath, numberOfFiles, callback) => {
    fs.mkdir(dirPath, { recursive: true }, (err) => {
        if (err) {
            callback(err)
        }
        else {
            let offset = 0;
            for (let fileNum = 1; fileNum <= numberOfFiles; fileNum++) {
                fs.writeFile(`${dirPath}/file${fileNum}.json`, "Hi", (err) => {
                    if (err) {
                        callback(err)
                    } else {
                        setTimeout(() => {
                            fs.unlink(`${dirPath}/file${fileNum}.json`, (err) => {
                                if (err) {
                                    callback(err)
                                } else {
                                    //console.log(`file${fileNum}.json`);
                                    callback(null, `file${fileNum}.json`)
                                }
                            })
                        }, 2000 + offset)
                        offset += 2000;
                    }
                })
            }
        }
    })
}