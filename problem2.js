const fs = require("fs");
const path = require("path")
const folderPath = path.join(__dirname, "./uppercase")
const filePath = path.join(__dirname, "./data/lipsum.txt")

module.exports.problem2 = (callback) => {
    readFile(filePath, (err, data) => {
        if (err) {
            callback(err)
        } else {
            writeFile(`${folderPath}/uppercase.txt`, data.toUpperCase(), (err) => {
                if (err) {
                    callback(err)
                } else {
                    writeFile(`${folderPath}/filenames.txt`, "uppercase.txt", (err) => {
                        if (err) {
                            callback(err)
                        } else {
                            readFile(`${folderPath}/uppercase.txt`, (err, data) => {
                                if (err) {
                                    callback(err)
                                } else {
                                    //console.log(data);
                                    let sentence = data.toLowerCase().split(". ")
                                    //console.log(sentence);
                                    writeFile(`${folderPath}/lowercase.txt`, JSON.stringify(sentence), (err) => {
                                        if (err) {
                                            callback(err)
                                        } else {
                                            fs.appendFile(`${folderPath}/filenames.txt`, "\nlowercase.txt", (err) => {
                                                if (err) {
                                                    callback(err)
                                                } else {
                                                    readFile(`${folderPath}/lowercase.txt`, (err, data) => {
                                                        if (err) {
                                                            callback(err)
                                                        } else {
                                                            //console.log(typeof data);
                                                            let parseData = JSON.parse(data)
                                                            parseData.sort()
                                                            //console.log(parseData);
                                                            writeFile(`${folderPath}/sort.txt`, JSON.stringify(parseData), (err) => {
                                                                if (err) {
                                                                    callback(err)
                                                                } else {
                                                                    fs.appendFile(`${folderPath}/filenames.txt`, "\nsort.txt", (err) => {
                                                                        if (err) {
                                                                            callback(err)
                                                                        } else {
                                                                            readFile(`${folderPath}/filenames.txt`, (err, data) => {
                                                                                if (err) {
                                                                                    callback(err)
                                                                                } else {
                                                                                    //console.log(typeof data);
                                                                                    let fileNamesData = data.split("\n")
                                                                                    //console.log(fileNamesData);
                                                                                    let offset = 0;
                                                                                    fileNamesData.forEach((fileName) => {
                                                                                        setTimeout(() => {
                                                                                            fs.unlink(`${folderPath}/${fileName}`, (err) => {
                                                                                                if (err) {
                                                                                                    callback(err)
                                                                                                } else {
                                                                                                    callback(null, `${fileName}`)
                                                                                                }
                                                                                            })
                                                                                        }, 2000 + offset)
                                                                                        offset += 2000;
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}
function readFile(path, callback) {
    fs.readFile(path, "utf8", (err, data) => {
        if (err) {
            callback(err, null)
        } else {
            callback(null, data)
        }

    })
}
function writeFile(folderPath, fileData, callback) {
    fs.writeFile(folderPath, fileData, (err) => {
        if (err) {
            callback(err)
        } else {
            callback(null)
        }
    })
}